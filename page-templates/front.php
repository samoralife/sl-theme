<?php
/*
Template Name: Front
*/
get_header(); ?>

<header class="front-hero" role="banner">
	<div class="marketing">
		<div class="tagline">
			<h1><?php echo html_entity_decode(get_bloginfo('description')); ?></h1>
		</div>
		<?php get_template_part( 'template-parts/bkg-video' ); ?>

	</div>

</header>

<section class="cta clearfix"><?php get_template_part( 'template-parts/morning-magic-cta' ); ?></section>

<section class="benefits">
	<div class="morelove">
<header>
<h1>Want more love in your life?</h1> 
<h2>Start here!</h2>
</header>

	<div class="events">
		<img src="http://samoralife.com/wp-content/uploads/2018/03/events_courses.jpg" alt="events and courses">
		<h3>Events + Courses</h3>
		<p>Learn about events, workshops and courses that will help you LOVE yourself FIRST!</p>
		<a href="http://samoralife.com/events_courses/"><button class="button">Start Learning!</button></a>
	</div>

	<div class="membership">
		<img src="http://samoralife.com/wp-content/uploads/2018/03/MEMBERSHIP.jpg" alt="membership">
		<h3>Membership</h3>
		<p>Gain non-stop love + support with exclusive practices created just for YOU!</p>
		<button class="button">Become a Self-Lovie!</button>
	</div>

	<div class="community">
		<img src="http://samoralife.com/wp-content/uploads/2018/03/community-1.jpg" alt="community">
		<h3>Community</h3>
		<p>Join our Facebook group of beautiful women who have, give, and receive love!</p>
		<a href="https://facebook.com/samoralife" target="_blank"> <button class="button">Join the Love!</button> </a>
	</div>

				</div>
</section>
<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="intro" role="main">
	<div class="fp-intro">

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
			<footer>
				<?php
					wp_link_pages(
						array(
							'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
							'after'  => '</p></nav>',
						)
					);
				?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php do_action( 'foundationpress_page_before_comments' ); ?>
			<?php comments_template(); ?>
			<?php do_action( 'foundationpress_page_after_comments' ); ?>
		</div>

	</div>

</section>
<?php endwhile;?>
<?php do_action( 'foundationpress_after_content' ); ?>

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="lyf" role="main">

	<div class="content">
	<header>
		<h1 class="highlight">Love Yourself First!</h1>
		<h4>If you ever need a reminder on how to put self-love into practice, come right ova here...</h4>
	</header>
	<?php do_action( 'foundationpsress_page_before_entry_content' ); ?>
		<div class="blogposts">
			<?php $i=1; ?>
			<?php $args = array( 'post_type' => 'post', 'posts_per_page' => 5 );
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();?>

				<div class="posts post-<?php echo $i++; ?>">
					<a href="<?php the_permalink();?>">
					
					<?php if ( has_post_thumbnail()) { ?>

						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );$url = $thumb['0'];?>
			<div   <?php post_class('visual overlay'); ?> style="background-image: url(<?php echo esc_url($url); ?>)">
			<h3><?php the_title();?></h3>			
		</div>
		
						<div class="postinfo">
						<div class="divider"></div>	
							<span><?php the_excerpt();?></span>
						</div>
						<?php } else { ?>
							<div class="noimg postinfo">
							<h3><?php the_title();?></h3>
							<div class="divider"></div>	
							<span><?php the_excerpt();?></span>
							</div>
						<?php } ?>
					</a>
				</div>
			<?php endwhile; endif; wp_reset_postdata();?> 
		</div>

	</div>
</section>
<?php endwhile;?>
<?php do_action( 'foundationpress_after_content' ); ?>
<section class="feed">
<div id="instafeed"></div>
</section>

<?php get_footer();
