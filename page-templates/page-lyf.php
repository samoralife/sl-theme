<?php
/*
Template Name: Love Yourself First
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image-lyf' ); ?>

<div id="page-events-courses" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
      <div class="entry-content quotable">
       <section>
           <h2>Sometimes, you have to slow down in order to speed up. </h2>
           </section>
           </div>
          <div class="entry-content sell">
           <section>
           <p>With life pulling you in many directions 24/7, it can be difficult to carve out quality time for yourself. And when there's no time for self, you begin to loose the clarity, connection and confidence that aligns you with your higher purpose and power.</p>
<p>For the next 5 weeks, you will SLOW DOWN, bring awareness to the key areas in your life and bring more attention to the area that needs more love.</p>
<div class="callouts"><span><?php get_template_part( 'template-parts/svg-icon-home' ); ?></span><p>Home</p></div>
<div class="callouts"><span><?php get_template_part( 'template-parts/svg-icon-health' ); ?></span><p>Health</p></div>
<div class="callouts"><span><?php get_template_part( 'template-parts/svg-icon-spirit' ); ?></span><p>Spirit</p></div>
<div class="callouts"><span><?php get_template_part( 'template-parts/svg-icon-relationships' ); ?></span><p>Relationships</p></div>
<div class="callouts"><span><?php get_template_part( 'template-parts/svg-icon-finance' ); ?></span><p>Finance</p></div>
<div class="callouts"><span><?php get_template_part( 'template-parts/svg-icon-service' ); ?></span><p>Service/Work</p></div>
           </section>
               </div>
           <div class="entry-content extras overlay">
             <section>
                 <div class="title">
           <h2>You're Worth More.</h2>
           <h4>In addition to the 50-page workbook, you will also gain:</h4>
           </div>
           <div class="highlights">
               <h4>Self-Care Practices</h4>
               <p>Know the best ways to take care of yourself during times of need</p>
               </div>

               <div class="highlights">
               <h4>Increased Focus</h4>
               <p>Find your priorities and how to recognize (and avoid) the noise</p>
               </div>

              <div class="highlights">
                <h4>Morning Practice</h4>
               <p>Get a morning routine to set you up for success each day</p>
               </div>

               <div class="highlights">
               <h4>Positive Change</h4>
               <p>Be supported in the changes you want to see made and work towards it</p>
               </div>

               <div class="highlights">
               <h4>Clarity</h4>
               <p>Understanding what you want and being intentional about seeing it manifest</p>
               </div>

               <div class="highlights">
               <h4>A Community of Supportive Women</h4>
               <p>Join a community of like-minded women who are looking to love like you!</p>
               </div>

               <div class="highlights">
               <h4>Time Management</h4>
               <p>Learn how to make the most of your time with emphasis on your priorities</p>
               </div>

               <div class="highlights">
               <h4>Reading Guide</h4>
               <p>Continue to learn far beyond this course with a self-love reading list!</p>
               </div>

           </section>
           </div>
           <div class="entry-content testimonials">
           <section>
                 <div class="title">
           <h2>Growth Looks Good on You.</h2>
           <h4>There's nothing like the feeling of accomplishing something meaningful.</h4>
           </div>
<div class="testimony">
<blockquote>SamoraLife helped me in becoming my full purposeful self. She showed me how to make my mental, spiritual and physical health a priority and gave me resources to elevate my awareness of who I am and who I want to become.
</blockquote>
<p>Shanay P., NYC</p>
</div>
<div class="testimony">
<blockquote>
After taking this course, I learned how to make myself a priority so that I can be happy. When I am happy, I am able to share more of myself with those I love and the world.
</blockquote>
<p>Madison E., CA</p>
</div>

           </section>
               </div>
           <div class="entry-content outro overlay">
           <section>
<div class="title">
          <h1 class="entry-title">Invest In Yourself First</h1>
		  </div>
		  <div class="subtitle">
		  <h3>And get a lifetime of love.</h3>
		   </div>
		  <div class="cta">
		  <a href="http://samoralife.teachable.com/"><button>Get Started Now</button></a>
		</section>
</div>
      </div>
          </div>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();
