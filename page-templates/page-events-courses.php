<?php
/*
Template Name: Events and Courses
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="page-events-courses" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
      <div class="entry-content upcoming_events">
       <section>
					 <h2>Upcoming Events</h2>
					 
							<?php $loop = new WP_Query( array( 'post_type' => 'event_listing') ); ?>
							
							<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
									<div class="eventlist">
									<?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</h3></p>' ); ?>
</div>
					
							<?php endwhile; ?>
							<div class="see_all">
					 <a class="button" href="<?php echo site_url(); ?>/events">See All</a>
</div>
           </section>
					 </div>
					 
					 <div class="divider"></div>	
          <div class="entry-content courses">
           <section>
					 <h2>Courses</h2>
					 
           <?php $loop = new WP_Query( array( 'post_type' => 'course') ); ?>
					 <div class="courselist">
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
<?php echo '<a href="' . get_post_meta($post->ID, 'course_url', true) . '" target="_blank" >' . get_the_post_thumbnail( get_the_ID(), 'full' ); '</a>'?>
</div>  
<?php endwhile; ?>

		</section>
</div>
      </div>
          </div>
  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();
