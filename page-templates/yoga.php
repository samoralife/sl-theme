<?php
/*
Template Name: Moga
*/
get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="page-full-width" role="main">

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
  <article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

      <?php do_action( 'foundationpress_page_before_entry_content' ); ?>
      <div class="entry-content about-practice">
          <section>
          <?php the_content(); ?>
            </section>
      </div>
      <div class="entry-content youtube-channel">
        <section>
        <iframe id="youtube_video" width="100%" height="500" frameborder="0" allowfullscreen></iframe>
</section>
<script>
var channelID = "UCeAxB1ahxn5lv6H6dR6GUDA";
$.getJSON('https://api.rss2json.com/v1/api.json?rss_url=https%3A%2F%2Fwww.youtube.com%2Ffeeds%2Fvideos.xml%3Fchannel_id%3D'+channelID, function(data) {
   var link = data.items[0].link;
   var id = link.substr(link.indexOf("=")+1);
    $("#youtube_video").attr("src","https://youtube.com/embed/"+id + "?controls=0&showinfo=0&rel=0");
});
</script>

      </div>
      <div class="entry-content yoga-imgs">
          <section>
         <div class="yogi">
          <img src="http://samoralife.com/wp-content/uploads/2018/01/C11SL_preview.jpg" />
        </div>
        <div class="yogi">
          <img src="http://samoralife.com/wp-content/uploads/2018/02/yoga.jpg" />
          </div>
        <div class="yogi">
          <img src="http://samoralife.com/wp-content/uploads/2018/01/B12SL_preview.jpg" />
          </div>
        <div class="yogi">
          <img src="http://samoralife.com/wp-content/uploads/2018/01/A9_SL_preview.jpg" />
          </div>
        <div class="yogi">
          <img src="http://samoralife.com/wp-content/uploads/2018/01/A8_SL.jpg" />
          </div>
        </section>
      </div>

  </article>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>

</div>

<?php get_footer();
