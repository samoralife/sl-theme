<?php
// Adding excerpt for page
add_post_type_support( 'page', 'excerpt' );

//Infinite Scroll
add_theme_support( 'infinite-scroll', array(
    'container' => 'content',
    'footer' => 'page',
) );

// Courses register custom post type to work with
function lc_create_post_type() {
    // set up labels
    $labels = array (
    'name' => 'Courses',
    'singular_name' => 'Course',
    'add_new' => 'Add New Course',
    'add_new_item' => 'Add New Course',
    'edit_item' => 'Edit Course',
    'new_item' => 'New Course',
    'all_items' => 'All Courses',
    'view_item' => 'View Course',
    'search_items' => 'Search Courses',
    'not_found' => 'No Courses Found',
    'not_found_in_trash' => 'No Courses found in Trash',
    'parent_item_colon' => '',
    'menu_name' => 'Courses',
    );
    //register post type
    register_post_type ( 'course', array(
    'labels' => $labels,
    'has_archive' => true,
    'public' => true,
    'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
    'taxonomies' => array( 'post_tag', 'category' ),
    'exclude_from_search' => false,
    'capability_type' => 'post',
    'rewrite' => array( 'slug' => 'courses' ),
    )
    );
    }
    add_action( 'init', 'lc_create_post_type' );

//Page Slug Body Class
function add_slug_body_class( $classes ) {
    global $post;
    if ( isset( $post ) ) {
    $classes[] = $post->post_name;
    }
    return $classes;
    }
    add_filter( 'body_class', 'add_slug_body_class' );