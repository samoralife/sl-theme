<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
	<header <?php post_class('featured-hero overlay'); ?> role="banner" data-interchange="[<?php the_post_thumbnail_url('featured-small'); ?>, small], [<?php the_post_thumbnail_url('featured-medium'); ?>, medium], [<?php the_post_thumbnail_url('featured-large'); ?>, large], [<?php the_post_thumbnail_url('featured-xlarge'); ?>, xlarge]">
    <div class="intro">
	<div class="entry-title">
    <h1><?php the_title(); ?></h1>
	  </div>
	  <div class="subtitle">
	  <h2>5 Weeks of Self-Care to Increase Confidence, Focus and Worth</h2>
	   </div>
	  <div class="cta">
	  <a href="http://samoralife.teachable.com/"><button>Get Started Now</button></a>
</div>
</div>
	</header>
<?php endif;
