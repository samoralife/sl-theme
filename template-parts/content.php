<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>



	
	
	<?php if ( is_single() ) { ?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="entry-content">
		<?php the_content(); ?>
		<?php edit_post_link( __( '(Edit)', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
	</div>
</article>

		<?php } else { ?>
			
		<?php if ( has_post_thumbnail()) { ?>
			<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' );$url = $thumb['0'];?>
			<div <?php post_class(); ?> >
			<div  class="visual overlay" style="background-image: url(<?php echo esc_url($url); ?>)">
			<?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );?>	
		</div>
			<div class="divider"></div>	
		
			<div class="postinfo">
			<?php the_excerpt();?>
			<div class="category"><?php the_category(', '); ?></div>
		</div>
			</div>
			<?php } else { ?>
				<div <?php post_class(); ?>>
				<?php the_title( '<h3 class="entry-title noimg"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );?>
			<div class="divider"></div>
			<div class="postinfo">
			<?php the_excerpt();?>
			</div>
			</div>

	<?php } ?>

			
	
	

			<?php } ?>
			<footer>
		<?php
			wp_link_pages(
				array(
					'before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ),
					'after'  => '</p></nav>',
				)
			);
		?>
		<?php $tag = get_the_tags(); if ( $tag ) { ?><p><?php the_tags(); ?></p><?php } ?>
	</footer>
	

	