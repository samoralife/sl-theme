<?php
// If a featured image is set, insert into layout and use Interchange
// to select the optimal image size per named media query.
if ( has_post_thumbnail( $post->ID ) ) : ?>
	<header <?php post_class('featured-hero overlay'); ?> role="banner" data-interchange="[<?php the_post_thumbnail_url('featured-small'); ?>, small], [<?php the_post_thumbnail_url('featured-medium'); ?>, medium], [<?php the_post_thumbnail_url('featured-large'); ?>, large], [<?php the_post_thumbnail_url('featured-xlarge'); ?>, xlarge]">
    <div class="entry-title">
    <h3><span class="highlight">|</span> Latest</h3>
        <h1><?php the_title(); ?></h1>
    <a class="button" href="<?php echo get_permalink(); ?>"> Read More...</a>
</div>
	</header>
<?php endif;
